package me.codeandroid.examplevoip;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;

public class PlaceCallActivity extends BaseActivity implements SinchService.StartFailedListener{
    private Button mCallButton;
    private EditText mCallName;
    FirebaseAuth mAuth;
    String extra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_call);

        mAuth = FirebaseAuth.getInstance();

        final FirebaseUser user = mAuth.getCurrentUser();
        String userName = user.getUid();



        Log.d("User", user.getUid().toString());
        String callerId = user.getUid().toString();
        //info from CallActivity
        //  Intent intent = getIntent();
        //   callerId = intent.getStringExtra("callerId");
        extra = getIntent().getStringExtra("rextra");
        Log.d("rextra", extra);
        Log.d("CallerId", callerId);
        Log.d("RecipientID", extra);

        mCallButton = (Button) findViewById(R.id.callButton);
        mCallButton.setEnabled(false);
        mCallButton.setOnClickListener(buttonClickListener);

        Button stopButton = (Button) findViewById(R.id.stopButton);
        stopButton.setOnClickListener(buttonClickListener);

    }
    // invoked when the connection with SinchServer is established
    @Override
    protected void onServiceConnected () {
        TextView userName = (TextView) findViewById(R.id.loggedInName);
        userName.setText(getSinchServiceInterface().getUserName());
        mCallButton.setEnabled(true);
        getSinchServiceInterface().setStartListener(this);

    }

    @Override
    public void onDestroy () {
        if (getSinchServiceInterface() != null) {
            getSinchServiceInterface().stopClient();
        }
        super.onDestroy();
    }

    //to kill the current session of SinchService
    private void stopButtonClicked () {
        if (getSinchServiceInterface() != null) {
            getSinchServiceInterface().stopClient();
        }
        finish();
    }

    //to place the call to the entered name
    private void callButtonClicked () {
        String userName = extra;
        Log.d("Extra", extra);
        if (userName.isEmpty()) {
            Toast.makeText(this, "Please enter a user to call", Toast.LENGTH_LONG).show();
            return;
        }
        Log.d("Call button Clicked", userName);
        Call call = getSinchServiceInterface().callUserVideo(extra);
        String callId = call.getCallId();
        Log.d("Call ID", callId);
        Intent callScreen = new Intent(this, CallScreenActivity.class);
        callScreen.putExtra(SinchService.CALL_ID, callId);
        startActivity(callScreen);
    }


    private View.OnClickListener buttonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.callButton:
                    callButtonClicked();
                    break;

                case R.id.stopButton:
                    stopButtonClicked();
                    break;

            }
        }
    };

    @Override
    public void onStartFailed(SinchError error) {

    }

    @Override
    public void onStarted() {

    }

}
