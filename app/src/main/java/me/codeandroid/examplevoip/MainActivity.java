package me.codeandroid.examplevoip;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.sinch.android.rtc.SinchError;

import java.util.ArrayList;

public class MainActivity extends BaseActivity implements SinchService.StartFailedListener {
    private TextView mTextMessage, mUserUidTextView;
    FirebaseAuth mAuth;
    public ArrayList<String> arr;
    public ArrayAdapter adapter;
    private Button mgetlist, mLogoutButton;
    private ProgressBar mCallActivityProgressBar;

    private static final int PERMISSION_CALLBACK_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    String[] permissionsRequired = new String[]{android.Manifest.permission.ACCESS_NETWORK_STATE, android.Manifest.permission.INTERNET, android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.RECORD_AUDIO,
            android.Manifest.permission.CAMERA,
            Manifest.permission.MODIFY_AUDIO_SETTINGS};
    private boolean sentToSettings = false;
    private SharedPreferences sharedPref, permissionStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();

        if (user == null) {
            startActivity(new Intent(MainActivity.this, SignupActivity.class));
            finish();
        }
        Log.d("Current User", user.toString());
        Log.d("current User uid", user.getUid());

        mgetlist = findViewById(R.id.getlist);
        mUserUidTextView = findViewById(R.id.userUidTextView);
        mCallActivityProgressBar = findViewById(R.id.callActivityProgressBar);
        mCallActivityProgressBar.setVisibility(View.INVISIBLE);
        mLogoutButton = findViewById(R.id.logoutButton);

        permissionStatus = getSharedPreferences("permissionStatus",MODE_PRIVATE);
        Log.d("onCreate", "OnCreate Method Finished");

        //    getList();




    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("onResume", "onResume Method Finished");
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        mgetlist = findViewById(R.id.getlist);
        mUserUidTextView = findViewById(R.id.userUidTextView);
        mUserUidTextView.setText(mAuth.getCurrentUser().getUid());
        mCallActivityProgressBar = findViewById(R.id.callActivityProgressBar);
        mCallActivityProgressBar.setVisibility(View.INVISIBLE);
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);
        String userName = mAuth.getCurrentUser().getUid();
        sharedPref = getSharedPreferences("Sinch",MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("firebasekey", userName);
        editor.commit();
        getList();

        mLogoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });

        updateToken();
    }

    private void updateToken() {
            sharedPref = getSharedPreferences("Sinch", MODE_PRIVATE);
            String refreshedToken = sharedPref.getString("tokenId", "");
            Log.d("Pref Token", refreshedToken);
            String userUid = mAuth.getCurrentUser().getUid();
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
            ref.child("users").child(userUid).child("tokenId").setValue(refreshedToken);

    }

    private void logout() {
        mAuth.signOut();
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
        finish();
    }

    private void getList() {
        mgetlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //permissions functions
                if(ActivityCompat.checkSelfPermission(MainActivity.this, permissionsRequired[0]) != PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(MainActivity.this, permissionsRequired[1]) != PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(MainActivity.this, permissionsRequired[2]) != PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(MainActivity.this, permissionsRequired[3]) != PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(MainActivity.this, permissionsRequired[4]) != PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(MainActivity.this, permissionsRequired[5]) != PackageManager.PERMISSION_GRANTED
                        ){
                    if(ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,permissionsRequired[0])
                            || ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,permissionsRequired[1])
                            || ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,permissionsRequired[2])
                            || ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permissionsRequired[3])
                            || ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permissionsRequired[4])
                            || ActivityCompat.shouldShowRequestPermissionRationale( MainActivity.this, permissionsRequired[5])
                            ){
                        //Show Information about why you need the permission
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Need Multiple Permissions");
                        builder.setMessage("This app needs Camera and Location permissions.");
                        builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                ActivityCompat.requestPermissions(MainActivity.this,permissionsRequired,PERMISSION_CALLBACK_CONSTANT);
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        builder.show();
                    }
                    else if (permissionStatus.getBoolean(permissionsRequired[0],false)) {
                        //Previously Permission Request was cancelled with 'Dont Ask Again',
                        // Redirect to Settings after showing Information about why you need the permission
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Need Multiple Permissions");
                        builder.setMessage("This app needs Camera and Location permissions.");
                        builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                sentToSettings = true;
                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                intent.setData(uri);
                                startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                                Toast.makeText(getBaseContext(), "Go to Permissions to Grant  Camera and Location", Toast.LENGTH_LONG).show();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        builder.show();
                    }  else {
                        //just request the permission
                        ActivityCompat.requestPermissions(MainActivity.this , permissionsRequired,PERMISSION_CALLBACK_CONSTANT);
                    }

                    SharedPreferences.Editor editor = permissionStatus.edit();
                    editor.putBoolean(permissionsRequired[0],true);
                    editor.commit();
                } else {
                    //You already have the permission, just go ahead.
                    Log.d("Permissions", "Permissions are successfuls");
                    getButton();
                }
                Log.d("Permissions are gotten", "Permissions are granted");
                ///////////////////////   getButton();

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == PERMISSION_CALLBACK_CONSTANT){
            //check if all permissions are granted
            boolean allgranted = false;
            for(int i=0;i<grantResults.length;i++){
                if(grantResults[i]==PackageManager.PERMISSION_GRANTED){
                    allgranted = true;
                } else {
                    allgranted = false;
                    break;
                }
            }

            if(allgranted){
                proceedAfterPermission();
            } else if(ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,permissionsRequired[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,permissionsRequired[1])
                    || ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,permissionsRequired[2])
                    || ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permissionsRequired[3])
                    || ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permissionsRequired[4])
                    || ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permissionsRequired[5])
                    ){
                // txtPermissions.setText("Permissions Required");
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Need Multiple Permissions");
                builder.setMessage("This app needs Camera and Location permissions.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(MainActivity.this,permissionsRequired,PERMISSION_CALLBACK_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                Toast.makeText(getBaseContext(),"Unable to get Permission",Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PERMISSION_SETTING) {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                proceedAfterPermission();
            }
        }
    }

    private void proceedAfterPermission() {
        Toast.makeText(getBaseContext(), "We got All Permissions", Toast.LENGTH_LONG).show();
        Log.d("Permissions", "Permissions are successfuls, proceeded after permissions");
        getButton();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (sentToSettings) {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                proceedAfterPermission();
            }
        }
    }

    private void getButton() {
        String userName = mAuth.getCurrentUser().getUid();
        System.out.println(userName);
        if (!getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().startClient(userName);
        } else {

            getTeachersList();
        }
    }

    @Override
    protected void onServiceConnected() {
        getSinchServiceInterface().setStartListener(this);
    }

    private void getTeachersList() {
        mCallActivityProgressBar.setVisibility(View.VISIBLE);
        final DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("users");
        final ListView listView = (ListView) findViewById(R.id.usersListView);

        //Query for Teacher
        final ArrayList<String> items = new ArrayList<String>();

        String teacherUrl = "https://examplevoip.firebaseio.com/users";
        DatabaseReference teacherUrlRef = FirebaseDatabase.getInstance().getReferenceFromUrl(teacherUrl);

        teacherUrlRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    for (DataSnapshot teacherlist : dataSnapshot.getChildren()){
                        String teacherName = teacherlist.child("displayName").getValue().toString();
                   //     Log.d("Teacher Name", teacherlist.child("Display Name").getValue().toString());
                        items.add(teacherName);
                    }
                    ArrayAdapter arrayAdapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1, items);
                    listView.setAdapter(arrayAdapter);

                    mCallActivityProgressBar.setVisibility(View.INVISIBLE);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            //listView.getItemAtPosition(position);
                            Log.d("Item Clicked", listView.getItemAtPosition(position).toString());
                            Query userQuery = usersRef.orderByChild("displayName").equalTo(listView.getItemAtPosition(position).toString());
                            userQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()){
                                        Log.d("Qurey User", dataSnapshot.getValue().toString());
                                        for (DataSnapshot recipientID : dataSnapshot.getChildren()){
                                         //   Log.d("Recipient ID", recipientID.child("UserID").getValue().toString());
                                            Intent i = new Intent(MainActivity.this, PlaceCallActivity.class);
                                            i.putExtra("rextra", recipientID.child("userUid").getValue().toString());
                                            startActivity(i);
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    mCallActivityProgressBar.setVisibility(View.INVISIBLE);
                                }
                            });
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


/*
        final ArrayList<String> friends = new ArrayList<String>();
        String Experturl = "https://english-4ba26.firebaseio.com/users/2GtzaEy6esfT9S06o6MEi7M2U642"; //DailEnglish924
        DatabaseReference teacherRef = FirebaseDatabase.getInstance().getReferenceFromUrl(Experturl);

        teacherRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Log.d("data Value", dataSnapshot.child("Display Name").getValue().toString());
                    String expertEmail = dataSnapshot.child("Display Name").getValue().toString();
                    Log.d("ExpertEmail", expertEmail);
//        usersRef.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(final DataSnapshot dataSnapshot) {
//
//                if (dataSnapshot.exists()) {
//                    for (DataSnapshot userlist : dataSnapshot.getChildren()) {
////                        Log.d("Users Node Datasnapshot", dataSnapshot.getChildren().toString());
////                        Log.d("Users Datasnapshot", dataSnapshot.toString());
////                        Log.d("Datasnapshot get value", dataSnapshot.getValue().toString());
////
////                        Log.d("userlist", userlist.getValue().toString());
////                        Log.d("User Names", userlist.child("Display Name").getValue().toString());
//                    //    Log.d("Userlist Value", userlist.child().getValue().toString());
//                        String userNames = userlist.child("Email").getValue().toString();
mCallActivityProgressBar.setVisibility(View.INVISIBLE);
                        friends.add(expertEmail);
                    }
                    ArrayAdapter arrayAdapter = new ArrayAdapter(CallActivity.this, android.R.layout.simple_list_item_1, friends);
                    listView.setAdapter(arrayAdapter);
                    mCallActivityProgressBar.setVisibility(View.INVISIBLE);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            //listView.getItemAtPosition(position);
                            Log.d("Item Clicked", listView.getItemAtPosition(position).toString());
                            Query userQuery = usersRef.orderByChild("Display Name").equalTo(listView.getItemAtPosition(position).toString());
                            userQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()){
                                        Log.d("Qurey User", dataSnapshot.getValue().toString());
                                        for (DataSnapshot recipientID : dataSnapshot.getChildren()){
                                            Log.d("Recipient ID", recipientID.child("UserID").getValue().toString());
                                            Intent i = new Intent(CallActivity.this, PlaceCallActivity.class);
                                            i.putExtra("rextra", recipientID.child("UserID").getValue().toString());
                                            startActivity(i);
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                mCallActivityProgressBar.setVisibility(View.INVISIBLE);
                                }
                            });
                        }
                    });
                }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mCallActivityProgressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(CallActivity.this, "Network Error. Try after sometime", Toast.LENGTH_SHORT).show();

            }

        });*/
    }

    @Override
    public void onStartFailed(SinchError error) {

    }

    @Override
    public void onStarted() {
        getTeachersList();
    }
}