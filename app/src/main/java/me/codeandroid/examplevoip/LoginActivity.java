package me.codeandroid.examplevoip;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.sinch.android.rtc.SinchError;

public class LoginActivity extends BaseActivity implements SinchService.StartFailedListener {
    private Button mibutton, mLoginButton;
    private EditText mEditUserName, mEditPassword;
    private FirebaseAuth mAuth;
    private TextView mGoToSignup, mGoToResetPassword;
    private ProgressBar mLoginActivityProgressBar;
    public static final int RequestPermissionCode = 7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //asking for permissions here
        if (Build.VERSION.SDK_INT != Build.VERSION_CODES.M) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //request for permissions
                requestPermissions(new String[]{android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.CAMERA, android.Manifest.permission.ACCESS_NETWORK_STATE, android.Manifest.permission.READ_PHONE_STATE}, 100);
            }
        }

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }

        mLoginActivityProgressBar = findViewById(R.id.loginActivityProgressBar);
        mLoginActivityProgressBar.setVisibility(View.INVISIBLE);
        mEditUserName = (EditText) findViewById(R.id.edit_user_name);
        mEditPassword = (EditText) findViewById(R.id.edit_password);

        mGoToSignup = findViewById(R.id.goToSignUp);
        mGoToSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSignup();
            }
        });

        mLoginButton = findViewById(R.id.loginButton);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLogin();
            }
        });

        mGoToResetPassword = findViewById(R.id.goToResetPassword);
        mGoToResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToResetPassword();
            }
        });

//        //asking & checking for permissions
//        if(CheckingPermissionIsEnabledOrNot())
//        {
//            Toast.makeText(LoginActivity.this, "All Permissions Granted Successfully", Toast.LENGTH_SHORT).show();
//            getstarted();
//            Log.d("Continue 1", "contineiu");
//        } else {
//            RequestMultiplePermission();
//        }

    }

    private void getstarted() {

    }


    private void goToResetPassword() {
        Toast.makeText(LoginActivity.this, "Reset Password Not Implemented", Toast.LENGTH_SHORT).show();
    }

    private void goToSignup() {
        startActivity(new Intent(LoginActivity.this, SignupActivity.class));
        finish();
    }

    private void checkLogin() {
        mLoginActivityProgressBar.setVisibility(View.VISIBLE);
        final String userName = mEditUserName.getText().toString();
        if (userName.length() == 0) {
            mLoginActivityProgressBar.setVisibility(View.INVISIBLE);
            mEditUserName.setError("please input user name");
            return;
        }

        final String password = mEditPassword.getText().toString();
        if (password.length() == 0) {
            mLoginActivityProgressBar.setVisibility(View.INVISIBLE);
            mEditPassword.setError("please input password");
            return;
        }

        mAuth.signInWithEmailAndPassword(userName, password).addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {

                    nextPage();


                } else {
                    mLoginActivityProgressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(getApplicationContext(), "Login Failed, Please Check Your Credentials", Toast.LENGTH_SHORT).show();
                }
            }

        });
    }

    private void nextPage() {
        // String userName = mEditUserName.getText().toString();
        mLoginActivityProgressBar.setVisibility(View.INVISIBLE);
        String userName = mAuth.getUid();
        Log.d("User ID", userName);
        if (!getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().startClient(userName);
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        } else {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }
    }

    @Override
    public void onStartFailed(SinchError error) {

    }

    @Override
    public void onStarted() {
        nextPage();
    }

}
