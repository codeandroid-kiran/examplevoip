package me.codeandroid.examplevoip;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sinch.android.rtc.SinchError;

import java.util.HashMap;
import java.util.Map;

public class SignupActivity extends BaseActivity implements SinchService.StartFailedListener {
    private EditText mNameEditText, mEmailEditText, mPasswordEditText;
    private Button mRegisterButton;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        //asking for permissions here
        if (Build.VERSION.SDK_INT != Build.VERSION_CODES.M) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.CAMERA, android.Manifest.permission.ACCESS_NETWORK_STATE, android.Manifest.permission.READ_PHONE_STATE},100);
            }
        }
        //initialize FirebaseAuth
        mAuth = FirebaseAuth.getInstance();
        //check if user is not null, if user is not null then direct him to Main Activity
        if (mAuth.getCurrentUser() != null) {
            startActivity(new Intent(SignupActivity.this, MainActivity.class));
            finish();
        }

        //assign ids edittexts and buttons
        mNameEditText = (EditText) findViewById(R.id.nameEditText);
        mEmailEditText = (EditText) findViewById(R.id.emailEditText);
        mPasswordEditText = (EditText) findViewById(R.id.passwordEditText);
        mRegisterButton = (Button) findViewById(R.id.registerButton);

        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });
    }
    //this method is invoked when the connection is established with the SinchService


    private void registerUser() {
        final String inputName = mNameEditText.getText().toString().trim();
        final String inputEmail = mEmailEditText.getText().toString().trim();
        String inputPassword = mEmailEditText.getText().toString().trim();

        if (TextUtils.isEmpty(inputName)){
            mNameEditText.setError("Name Field cannot be Blank. Enter your Name.");
            return;
        }
        if (TextUtils.isEmpty(inputEmail)){
            mEmailEditText.setError("Email Field cannot be Blanks. Put valid Email address for verification");
            return;
        }
        if (TextUtils.isEmpty(inputPassword)){
            mPasswordEditText.setError("Password field cannot be blank");
            return;
        }

        if (inputPassword.length() < 4){
            mPasswordEditText.setError("Password should be more then 4 character");
            return;
        }

        mAuth.createUserWithEmailAndPassword(inputEmail, inputPassword).addOnCompleteListener(SignupActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                mRegisterButton.setEnabled(false);
                if (task.isSuccessful()){
                    saveuserDetails(inputEmail, inputName, mAuth.getCurrentUser().getUid());
                    toNextPage();
                }else {
                    mRegisterButton.setEnabled(true);
                    Toast.makeText(SignupActivity.this, "Registration Failed "+ task.getException(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void toNextPage() {
        String userName = mAuth.getUid();

        if (!getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().startClient(userName);
        } else {
            startActivity(new Intent(SignupActivity.this, MainActivity.class));

            finish();
        }
    }

    //this method is invoked when the connection is established with the SinchService
    @Override
    protected void onServiceConnected() {
        getSinchServiceInterface().setStartListener(this);
    }

    private void saveuserDetails(String inputEmail, String inputName, String uid) {
        FirebaseUser user = mAuth.getCurrentUser();
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

        Map<String, String> map = new HashMap<String, String>();
        map.put("displayName", inputName);
        map.put("userUid", uid);
        map.put("userEmail", inputEmail);
        map.put("tokenId", "");
        ref.child("users").child(user.getUid()).setValue(map);
    }

    @Override
    public void onStartFailed(SinchError error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();

    }

    @Override
    public void onStarted() {
        toNextPage();
    }
}
